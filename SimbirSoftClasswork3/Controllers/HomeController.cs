﻿using SimbirSoftClasswork3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimbirSoftClasswork3.Controllers
{
    public class HomeController : Controller
    {
        LibraryContext context = new LibraryContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Сайт такой-то.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Страница наших контактов.";

            return View();
        }

        public ActionResult List()
        {
            ViewBag.Message = "Страница c листом книг которых нет.";
            
            return View(context.Librarys);
        }

        [HttpPost]
        public ActionResult Create(Library tmp)
        {
            ViewBag.Message = "Страница c листом книг которых нет.";

            return View();
        }

        [HttpGet]
        public ActionResult Add()
        {
            

            return View();
        }

        [HttpPost]
        public ActionResult Add(Library tmp)
        {
            context.Librarys.Add(tmp);
            context.SaveChanges();

            return View("List", context.Librarys);
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {


            return View(context.Librarys.Find(Id));

        }

        [HttpPost]
        public ActionResult Edit(Library tmp)
        {
            context.Librarys.Attach(tmp);
            IEnumerable<Library> Librarys = context.Librarys
                .Where(c => c.Id == tmp.Id)
                .AsEnumerable()
                 .Select(c => {
                     c.Name = tmp.Name;
                     c.Autor = tmp.Autor;
                    return c;
               });
            foreach (Library customer in Librarys)
            {
                context.Entry(customer).State = EntityState.Modified;
            }

            context.SaveChanges();

            return View("List", context.Librarys);
        }

        [HttpGet]
        public ActionResult Delete(int Id)
        {


            return View(context.Librarys.Find(Id));

        }

        [HttpPost]
        public ActionResult Delete(Library tmp)
        {
            context.Librarys.Attach(tmp);
            context.Librarys.Remove(tmp);

            context.SaveChanges();

            return View("List", context.Librarys);
        }

        [HttpGet]
        public ActionResult Details(int Id)
        {


            return View(context.Librarys.Find(Id));

        }
    }
}